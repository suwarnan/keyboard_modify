﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace WpfApplication1
{
    public partial class ResetButtonWindow : Form
    {
        string but,content,mapstring,path,Id;
        
        Connection_Query cq = new Connection_Query();
       

        public ResetButtonWindow(string button, string content, string mapstring, string path,string Id)
        {
            InitializeComponent();
            this.but = button;
            this.content = content;
            this.mapstring = mapstring;
            this.path = path;
            this.Id = Id;

           

            showContent();
        }

        public ResetButtonWindow()
        {
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string MessageBoxTitle = "View Path";
            string MessageBoxContent = path;

            if (path == null)
            {
                MessageBox.Show("Path Is Null Yet",MessageBoxTitle);
            }
            else
            {
                MessageBox.Show(MessageBoxContent, MessageBoxTitle);

            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

		private void groupBox1_Enter(object sender, EventArgs e)
		{

		}

		private void button11_Click(object sender, EventArgs e)
        {
            
            this.Close();

        }

        private void button10_Click(object sender, EventArgs e)
        {
           
                System.Windows.Forms.DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Are You Sure You Want Remove Key ?", "Remove Current Key Status", System.Windows.Forms.MessageBoxButtons.YesNo);
                if (dialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                if (checkBox1.Checked == true && checkBox2.Checked == true)
                {
                    String qu4 = "DELETE FROM Mapstring WHERE Id='" + Id + "'";
                    cq.executeQueries(qu4);

                    String qu5 = "DELETE FROM Run WHERE Id='" + Id + "'";
                    cq.executeQueries(qu5);

                  

                    MessageBox.Show("delete All Data From Database");

                    checkBox1.Checked = false;
                    checkBox2.Checked = false;
                }
               else if (checkBox1.Checked == true)
                {

                    if (Id != "")
                    {
                        String qu = "DELETE  FROM Mapstring WHERE Id='" + Id + "'";
                        cq.executeQueries(qu);

                       

                        MessageBox.Show("Delete successfully");

                        checkBox1.Checked = false;

                    }

                }
                else if (checkBox2.Checked == true)
                    {
                        if (Id != "")
                        {
                            String qu6 = "DELETE  FROM Run WHERE Id='" + Id + "'";
                            cq.executeQueries(qu6);

                            MessageBox.Show("Delete successfully");

                            checkBox2.Checked = false;

                     
                    }
                    }
                String qu2 = "SELECT * FROM Mapstring WHERE Id='" + Id + "'";
                SqlDataReader reader = cq.dataReader(qu2);
                string Id1="";
                string Id2="";
                while (reader.Read())
                {
                     Id1 = reader["Id"].ToString();

                }
                reader.Close();
                String qu1 = "SELECT * FROM Run WHERE Id='" + Id + "'";
                SqlDataReader reader1 = cq.dataReader(qu1);

                while (reader1.Read())
                {
                     Id2 = reader1["Id"].ToString();
                }
                reader1.Close();
                if (Id1=="" && Id2 == "")
                {
                    String qu3 = "DELETE FROM Button WHERE Id='" + Id + "'";
                        cq.executeQueries(qu3);
                }

            }
            System.Windows.Forms.Application.Restart();


        }

       

        private void button4_Click(object sender, EventArgs e)
        {
            string MessageBoxTitle = "Mapping Sting";
            string MessageBoxContent = mapstring;
            // MessageBox.Show(MessageBoxContent, MessageBoxTitle, MessageBoxButtons.YesNo);
            if (mapstring == null)
            {
                MessageBox.Show("Mapstring Is Null Yet",MessageBoxTitle);

            }
            else
            {
                MessageBox.Show(MessageBoxContent, MessageBoxTitle);
            }
           

        }

        private void showContent()
        {
			label2.Text = but;
			label3.Text = content;
			label1.Text = but;
			label4.Text = but;

        }


    }
}

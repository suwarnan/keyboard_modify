﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;



namespace WpfApplication1
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        string btn1="";
        string btnName="";
        string selectedkey = "";
        string keyName = "";
        string realName="";
       


       // MainWindow m;
        keyboardOne kbone = new keyboardOne();

        Connection_Query con = new Connection_Query();

        //String str = "Data Source=DESKTOP-M37RG4M\\SQLEXPRESS;Initial Catalog=keyboard;Integrated Security=True";
        //SqlConnection con;


        public MainWindow()
        {
          
            InitializeComponent();
            show_key();
        }
      

        //public MainWindow(string a)
        //00

        //    InitializeComponent();
        //    show_key();
        //}


        private void hai(object sender, RoutedEventArgs e)
        {

            var button = (Button)sender;
            var X = Canvas.GetLeft(button);
            var Y = Canvas.GetTop(button);
            Canvas.SetLeft(combo, X + 32);
            Canvas.SetTop(combo, Y + 32);
            combo.Visibility = Visibility.Visible;

            //if () {
            //    Shift.IsEnabled = false;
            //    ShiftCtrl.IsEnabled = false;
            //    AltCtrl.IsEnabled = false;
            //    Ctrl.IsEnabled = false;
            //}

            btn1 = button.Content.ToString();
            btnName = button.Name;

            if (Ctrl.IsChecked == true)
            {
                selectedkey = "ctrl";
            }
            else if (AltCtrl.IsChecked == true)
            {
                selectedkey = "AltCtrl";
            }
            else if (ShiftCtrl.IsChecked == true)
            {
                selectedkey = "ShiftCtrl";
            }
            else if (Shift.IsChecked == true)
            {
                selectedkey = "Shift";
            }
            KeyFinder key = new KeyFinder();
            realName = key.FindKey(btnName);
        }
        public string getKeyName(string btnName,string selectedkey)
        {
            if (selectedkey == "")
            {
                keyName = btnName;
            }
            if (selectedkey == "ctrl")
            {
                keyName = "^" + btnName;
            }
            if (selectedkey == "AltCtrl")
            {
                keyName = "!^" + btnName;
            }
            if (selectedkey == "ShiftCtrl")
            {
                keyName = "+^" + btnName;
            }
            if (selectedkey == "Shift")
            {
                keyName = "+" + btnName;
            }

            return keyName;

        }

        public void getRunPath(string btn,string realName,string selectkey, string userSelectedFilePath)
        {
          
            if (selectkey == "")
            {
                InsertRun(realName, userSelectedFilePath);
                

            }
            if (selectkey == "ctrl")
            {
                InsertRun("^" + realName, userSelectedFilePath);

            }
            if (selectkey == "AltCtrl")
            {
                InsertRun("!^" + realName, userSelectedFilePath);

            }
            if (selectkey == "ShiftCtrl")
            {
                InsertRun("+^" + realName, userSelectedFilePath);

            }
            if (selectkey == "Shift")
            {
                InsertRun("+" + realName, userSelectedFilePath);

            }
        }


        public void InsertRun(string btn,string userSelectedFilePath)
        {

            try
            {

               
                DateTime today = DateTime.Today;
              
                String query = "IF(NOT EXISTS(SELECT * FROM Button WHERE Name = '" + btn + "'))BEGIN INSERT INTO Button (Name) VALUES('" + btn + "')END";
                con.executeQueries(query);
               // SqlCommand cmd = new SqlCommand(query, con);
              //  int resultaat = cmd.ExecuteNonQuery();

                String query1 = "SELECT Id FROM Button WHERE Name = '" + btn + "'";
               SqlDataReader reader= con.dataReader(query1);
               // SqlCommand cmd1 = new SqlCommand(query1, con);
                //SqlDataReader reader = cmd1.ExecuteReader();
                string Id = "";
                while (reader.Read())
                {
                    Id = reader["Id"].ToString();
                }
                reader.Close();
                String query2 = "IF(NOT EXISTS(SELECT * FROM Run WHERE Id = '" + Id + "'))BEGIN INSERT INTO Run(Id,Application,Timestamp)VALUES('" + Id + "', '" + userSelectedFilePath + "','" + today + "')END ELSE BEGIN UPDATE Run SET Application='" + userSelectedFilePath + "',Timestamp = '" + today + "' WHERE Id= '" + Id + "' END";
                con.executeQueries(query2);
                // SqlCommand cmd2 = new SqlCommand(query2, con);
                //int resultat = cmd2.ExecuteNonQuery();

                con.closeConnection();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);

            }
        }



        public void GetValue(string btn, string realName,string oldcontent, string text, string content, string selectedkey)
        {
            var mapstring = text;
            var buttoncontent = content;
            //var btn2 = (Button)this.FindName(btn);

            //change the Button Name. use this code
            var mainWin = Application.Current.Windows.Cast<Window>().FirstOrDefault(window => window is MainWindow) as MainWindow;
            var btnn = (Button)mainWin.FindName(btn);
			if (buttoncontent == "")
			{
				btnn.Content = realName;
				addstringscript(btn, realName, text, realName, selectedkey);

			}
			else {
				btnn.Content = content;

				addstringscript(btn, realName, text, content, selectedkey);
			}

			
            

            //addstringscript(btn, text, content, selectedkey);

        }

        public void addstringscript(string btn,string realName, string text, string content, string selectedkeyValue)
        {
            kbone.addScript(btn,realName, text, content, selectedkeyValue);
           
        }

        public void searchword(string toSearch)
        {
            //MessageBox.Show(toSearch);
            //foreach (string line in File.ReadAllLines(@"Test.txt"))
            //{
            //    if (line.Contains(toSearch))
            //        //Console.WriteLine(line);
            //        MessageBox.Show(line);
            //}
        }

       //private void Insert(string button, string content, string map)
       // {
       //     kbone.save(button , content , map);
       // }


        private void item11(object sender, RoutedEventArgs e)
        {
            combo.Visibility = Visibility.Hidden;
            MapString win2 = new MapString(btn1, btnName,realName, selectedkey);
			win2.ShowDialog();
			
			win2.TopMost = true;
			




		}


        private void item12(object sender, RoutedEventArgs e)
        {

            combo.Visibility = Visibility.Hidden;               
            MapRun win3 = new MapRun(btnName, realName,selectedkey);
			win3.ShowDialog();
			win3.TopMost = true;
           
           
        }



        private void item13(object sender, RoutedEventArgs e)
        {
            string windowname = "reset";
			combo.Visibility = Visibility.Hidden;
			kbone.showKeyContent(realName, selectedkey, windowname);
        
            

            
           
        }

        private void item14(object sender, RoutedEventArgs e)
        {
            string windowname = "window4";
			combo.Visibility = Visibility.Hidden;
			kbone.showKeyContent(realName,selectedkey, windowname);
           
        }

        private void shift_Checked(object sender, RoutedEventArgs e)
        {
            ShiftCtrl.IsChecked = false;
            AltCtrl.IsChecked = false;
            Ctrl.IsChecked = false;

            ShiftCtrl.IsEnabled = false;
            AltCtrl.IsEnabled = false;
            Ctrl.IsEnabled = false;

            kbone.shiftCheacked();


        }


        private void ctrl_Checked_1(object sender, RoutedEventArgs e)
        {
            Shift.IsChecked = false;
            ShiftCtrl.IsChecked = false;
            AltCtrl.IsChecked = false;

            Shift.IsEnabled = false;
            ShiftCtrl.IsEnabled = false;
            AltCtrl.IsEnabled = false;



            kbone.ctrlCheacked();
           // combo.Visibility = Visibility.Hidden;
           
        }

       

        public void refr()
        {
            //MainWindow asa = new MainWindow();

            //new MainWindow().Show();
            //this.Close();

            //this.show_key();
            System.Windows.Forms.Application.Restart();
            this.Close();

        }

        public void refresh(object sender, RoutedEventArgs e)
        {

            /********************** get Curren running window name**********************************/
            //string name = GetType().Name;
            //MessageBox.Show(name);
            /**********************************************************************************************/

          

            //new MainWindow().Show();
            //this.Close();
            //this.show_key();
             System.Windows.Forms.Application.Restart();
            this.Close();
           


            //combo.Visibility = Visibility.Hidden;
        }

        private void altCtrl_Checked(object sender, RoutedEventArgs e)
        {
            Shift.IsChecked = false;
            ShiftCtrl.IsChecked = false;
            Ctrl.IsChecked = false;

            Shift.IsEnabled = false;
            ShiftCtrl.IsEnabled = false;
            Ctrl.IsEnabled = false;


            kbone.altCtrlCheacked();           
        }


        private void shiftCtrl_Checked(object sender, RoutedEventArgs e)
        {
            Shift.IsChecked = false;
            AltCtrl.IsChecked = false;
            Ctrl.IsChecked = false;

            Shift.IsEnabled = false;
            AltCtrl.IsEnabled = false;
            Ctrl.IsEnabled = false;

            kbone.shiftCtrlCheacked();
          
        }


        public void show_key()
        {           
            kbone.displayKey();
        }


        private void Generate_Click(object sender, RoutedEventArgs e)
        {
            string check = kbone.checkbutton();
            if (check != "")
            {
                kbone.applyClick();
            }
             else
            {
                MessageBox.Show("Please Modify Keyboard First !!", "Notification", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

       public void viewMappString()
        {
            MessageBox.Show("cheack");
        }

        private void combo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            combo.Visibility = Visibility.Hidden;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
                  
        }

        private void viewMappString(object sender, RoutedEventArgs e)
        {
            kbone.mappStringView();
        }

        private void viewRun(object sender, RoutedEventArgs e)
        {
            kbone.runView();
        }

        private void applyChange(object sender, RoutedEventArgs e)
        {
            string check = kbone.checkbutton();
            if (check != "")
            {
                if (MessageBox.Show("Are You Sure You Want Apply Keybord ?", "Apply To Current Keyboard", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    kbone.applyClick();
                }
            }
            else
            {
                MessageBox.Show("Please Modify Keyboard First !!", "Notification", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

      

		private void num_1_Click(object sender, RoutedEventArgs e)
		{

		}

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            string check = kbone.checkbutton();
            if (check != "")
            {
                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.FileName = "keyboard";
                dlg.DefaultExt = ".kb";
                dlg.Filter = "kb Files | *.kb";
                Nullable<bool> result = dlg.ShowDialog();
                if (result == true)
                {

                    string filename = dlg.FileName;
                    kbone.export(filename);
                    //kbone.open(filename);
                }
            }
            else
            {
                MessageBox.Show("Please Modify Keyboard First !!", "Notification", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Filter = "kb Files | *.kb";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                string filename = dlg.FileName;
                kbone.import(filename);
                refr();
            }
       }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Console app
                System.Environment.Exit(1);
            }
        }
        private void aboutus(object sender, RoutedEventArgs e)
        {
            aboutus a = new aboutus();
            a.ShowDialog();
        }
        private void resetKeyboard(object sender, RoutedEventArgs e)
        {
           if( MessageBox.Show("Are You Sure You Want Reset Keybord ?", "Delete Current Keyboard",MessageBoxButton.YesNo,MessageBoxImage.Question)==MessageBoxResult.Yes)   
            {
                kbone.cheackButtonName();
                refr();
            }
        }
        private void MenuItem_Click_4(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Do You Need to Create New Layout ? ", "New Layout", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                MainWindow m = new MainWindow();
                kbone.reset();
                this.Close();
                m.Show();
            }
        }
    }
    
}

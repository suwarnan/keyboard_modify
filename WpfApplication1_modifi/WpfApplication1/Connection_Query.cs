﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Windows;
using System.Windows.Controls;

namespace WpfApplication1
{
    class Connection_Query
    {
        string conectionString = "Data Source=.\\SQLEXPRESS;Initial Catalog=KeyMap;Integrated Security=True;MultipleActiveResultSets=true";
        SqlConnection con;
        SqlCommand cmd;

        public  Connection_Query()
        {
            con = new SqlConnection(conectionString);

            con.Open();
        }


        public void closeConnection()
        {
            con.Close();
        }

        public void executeQueries(string Query_)
        {
            //Insert , Update , Delete Queries (it is not return values)
            cmd = new SqlCommand(Query_ , con);
           cmd.ExecuteNonQuery(); 

        }

        public SqlDataReader dataReader(string Query_)
        {
            //Search Query(it is return value)
            cmd = new SqlCommand(Query_ , con);
            SqlDataReader dr = cmd.ExecuteReader(); 
            
            return dr; 
        }

        public object ShowDataInGridView(string Query_)
        {
            //show the data
            SqlDataAdapter dr = new SqlDataAdapter(Query_ , conectionString);
            DataSet ds = new DataSet();
            dr.Fill(ds);
            object dataum = ds.Tables[0];
            return dataum;
        }

    }
}
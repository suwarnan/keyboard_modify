﻿
using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Data.SQLite;
using System.Globalization;

namespace WpfApplication1

{
    class keyboardOne
    {
		
		string but;
        string content;
        string mapString;
        string path;

        string btnName = "";
        Connection_Query con_qu = new Connection_Query();


       // MainWindow mwindow = new MainWindow();

        public void save(string button, string content, string map)
        {
			try
			{
				DateTime today =DateTime.Now;
				
				//String query0 = "NOT EXISTS(SELECT * FROM Button WHERE Name = '" + replacesinglequote(button) + "')) BEGIN INSERT INTO Button (Name) VALUES('" + replacesinglequote(button) + "') END";

				String query = "INSERT INTO Button(Name) SELECT '" + replacesinglequote(button) + "' WHERE NOT EXISTS(SELECT 1 FROM Button WHERE Name = '" + replacesinglequote(button) + "')";
				con_qu.executeQueries(query);

				String query1 = "SELECT Id FROM Button WHERE Name = '" + replacesinglequote(button) + "'";
				SQLiteDataReader rd = con_qu.dataReader(query1);

				string Id = "";
				while (rd.Read())
				{
					Id = rd["Id"].ToString();
				}
				rd.Close();
				//String query11 = "NOT EXISTS(SELECT * FROM Mapstring WHERE Id = '" + Id + "'))BEGIN INSERT INTO Mapstring(Id,Button, Content, MapString, Timestamp)VALUES('" + Id + "', '" + replacesinglequote(button) + "','" + content + "', '" + map + "', '" + today + "')END ELSE BEGIN UPDATE Mapstring SET Button='" + replacesinglequote(button) + "', Content = '" + content + "',MapString = '" + map + "',Timestamp = '" + today + "' WHERE Id= '" + Id + "' END";
				String query2 = "INSERT OR REPLACE  INTO Mapstring(Id,Button, Content, MapString, Timestamp)VALUES('" + Id+"','"+replacesinglequote(button)+"','"+content+"','"+map+"','"+today.ToString()+"')" ;
				
				con_qu.dataReader(query2);

				con_qu.closeConnection();

				MessageBox.Show("MapString Successfully","",MessageBoxButton.OK,MessageBoxImage.Information);
			} catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
        }

		public string changeString(string s)
		{
			if (s == "'")
			{
				s = "''";
			}
			return s;
		}

		public string changeToSingelQuotation(string s)
		{
			if (s == "''")
			{

				s = "'";
			}
			return s;
		}


		public string replacesinglequote(string s)
        {
            if (s.EndsWith("'"))
                s = s.Remove(s.Length - 1, 1) + "''";
            return s;
        }

        public void displayKey()
        {
            try
            {
                
                String qu = "SELECT * FROM Mapstring";

                SQLiteDataReader sreader = con_qu.dataReader(qu);
               
                while (sreader.Read())
                {
                    var but = sreader["Button"].ToString();

                    if (but[0] != '+' && but[0] != '!' && but[0] != '^')
                    {
                        var mainWin = Application.Current.Windows.Cast<Window>().FirstOrDefault(window => window is MainWindow) as MainWindow;
                        KeyFinder key = new KeyFinder();

                        string butName = key.GetNames(but);
                        var btn = (Button)mainWin.FindName(butName);
                        btn.UpdateLayout();
                        btn.Content = sreader["Content"].ToString();
                        // btn.UpdateLayout();
                        //MessageBox.Show(btn.Content.ToString());
                    }
                    
                }
                sreader.Close();

            }
            catch (Exception e){
                MessageBox.Show(e.Message);
            }

        }




        public void shiftCheacked ()
        {
            try
            {
				
                String query = "SELECT * FROM Mapstring";

                SQLiteDataReader reader2 = con_qu.dataReader(query);
                string but = "";
                string cont = "";
                while (reader2.Read())
                {
                     but = reader2["Button"].ToString();
                     cont = reader2["Content"].ToString();
                
                if (but[0] == '+' && but[1] != '^')
                    {
                        but = but.Remove(0, 1);
                        but = getName(but);
                        var mainWin = Application.Current.Windows.Cast<Window>().FirstOrDefault(window => window is MainWindow) as MainWindow;
                        var btn = (Button)mainWin.FindName(but);

                    //btn.Content = reader1["Content"].ToString();

                    btn.Content = cont;

                    }
                }
                reader2.Close();



            }
            catch (NullReferenceException e)
            {
               MessageBox.Show(e.Message);
            }
        }

        public void ctrlCheacked()
        {
            try
            {
                String query = "SELECT * FROM Mapstring";
                SQLiteDataReader reader2 = con_qu.dataReader(query);
                string but = "";
                string cont = "";
                while (reader2.Read())
                {
                    but = reader2["Button"].ToString();
                    cont= reader2["Content"].ToString();
                

                if (but[0] == '^')
                    {
                     but = but.Remove(0, 1);
                     but = getName(but);
                        var mainWin = Application.Current.Windows.Cast<Window>().FirstOrDefault(window => window is MainWindow) as MainWindow;
                        var btn = (Button)mainWin.FindName(but);

                    btn.Content = cont;
                    }

                }
                reader2.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public void altCtrlCheacked()
        {
            try
            {
                String query = "SELECT * FROM Mapstring";
                
                SQLiteDataReader reader2 = con_qu.dataReader(query);
                string but = "";
                string cont = "";
                while (reader2.Read())
                {
                    but = reader2["Button"].ToString();
                    cont = reader2["Content"].ToString();
               
                if (but[0] == '!' && but[1] == '^')
                    {
                        but = but.Remove(0, 2);
                    but = getName(but);
                        var mainWin = Application.Current.Windows.Cast<Window>().FirstOrDefault(window => window is MainWindow) as MainWindow;
                        var btn = (Button)mainWin.FindName(but);

                        btn.Content = cont;
                    }

                }
                reader2.Close();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

        }

        public void shiftCtrlCheacked()
        {
            try
            {

                String query = "SELECT * FROM Mapstring";

                SQLiteDataReader reader2 = con_qu.dataReader(query);
                string but = "";
                string cont = "";
                while (reader2.Read())
                {

                    but = reader2["Button"].ToString();
                    cont = reader2["Content"].ToString();
                
                if (but[0] == '+' && but[1] == '^')
                    {
                        but = but.Remove(0, 2);
                    but = getName(but);
                        var mainWin = Application.Current.Windows.Cast<Window>().FirstOrDefault(window => window is MainWindow) as MainWindow;
                        var btn = (Button)mainWin.FindName(but);

                        btn.Content =cont;

                    }

                }
                reader2.Close();
            }
            catch(Exception e) {
                MessageBox.Show(e.Message);
            }
        }

        public string getName(string content)
        {
            String query = "SELECT * FROM KeyName Where Content ='"+ replacesinglequote(content) + "'";

            SQLiteDataReader reader1 = con_qu.dataReader(query);
            string name = "";
            while (reader1.Read())
            {
                name = reader1["Name"].ToString();
            }
            reader1.Close();
            if (name != "")
            {
                return name;
            }
            else return content;
        }

        public void applyClick()
        {

            
                File.WriteAllText(@"Test/Test.ahk", String.Empty);
                String query = "SELECT * FROM Button";

                SQLiteDataReader reader = con_qu.dataReader(query);

                //  SQLiteDataReader reader = cmd.ExecuteReader();
                string[] btn = new string[1000];
                string[] btnName = new string[1000];
                int i = 0;

                while (reader.Read())
                {
                    /*File.AppendAllText(@"Test/Test.ahk", "$" + reader["Button"] + "::" + Environment.NewLine);
                    File.AppendAllText(@"Test/Test.ahk", "\t Send, " + reader["MapString"] + Environment.NewLine);
                    File.AppendAllText(@"Test/Test.ahk", "return" + Environment.NewLine);*/

                    btn[i] = reader["Id"].ToString();
                    btnName[i] = reader["Name"].ToString();
                    i++;
                }
                reader.Close();
                for (int a = 0; a < i; a++)
                {
                    File.AppendAllText(@"Test/Test.ahk", "$" + btnName[a] + "::" + Environment.NewLine);
                    String query1 = "SELECT * FROM MapString where Id='" + btn[a] + "'";

                    // SQLiteCommand cmd1 = new SQLiteCommand(query1, con);
                    reader.Close();
                    // SQLiteDataReader reader1 = cmd1.ExecuteReader();

                    SQLiteDataReader reader1 = con_qu.dataReader(query1);

                    while (reader1.Read())
                    {
                        File.AppendAllText(@"Test/Test.ahk", "\t send, " + reader1["MapString"] + Environment.NewLine);
                    }
                    reader1.Close();

                    String query2 = "SELECT * FROM Run where Id='" + btn[a] + "'";
                    // SQLiteCommand cmd2 = new SQLiteCommand(query2, con);
                    reader1.Close();
                    // SQLiteDataReader reader2 = cmd2.ExecuteReader();

                    SQLiteDataReader reader2 = con_qu.dataReader(query2);

                    while (reader2.Read())
                    {

                        File.AppendAllText(@"Test/Test.ahk", "\t run, " + reader2["Application"] + Environment.NewLine);
                    }
                    reader2.Close();
                    File.AppendAllText(@"Test/Test.ahk", "Return " + Environment.NewLine);
               

                }

                System.Diagnostics.Process.Start(@"AutoHotkey.exe");
            MessageBox.Show("Applied Successfully", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            

        }

        public void export(string path)
        {
            StreamWriter sw = File.CreateText(path);
                
                using (sw)
                {
                
                    sw.WriteLine("{\n'keyboard':\n\t{\n\t'Button':\n\t\t[");
                     String query = "SELECT * FROM Button";
                    SQLiteDataReader reader = con_qu.dataReader(query);
                while (reader.Read())
                {
                    sw.WriteLine("\t\t{'Id':'" + reader["Id"].ToString() + "','Name':'" + slacerepace( reader["Name"].ToString()).Replace("'","___") + "'},");
                }
                    sw.WriteLine("\t\t],\n\t'MapString':\n\t\t[");
                    reader.Close();

                String query1 = "SELECT * FROM MapString";
                SQLiteDataReader reader1 = con_qu.dataReader(query1);
                while (reader1.Read()) {
                    
                    sw.WriteLine("\t\t{'Id':'" + reader1["Id"].ToString()+ "','Button':'"+ slacerepace(reader1["Button"].ToString()).Replace("'", "___") + "','MapString':'" + slacerepace(reader1["MapString"].ToString())+ "','Content':'" + slacerepace(reader1["Content"].ToString())+ "','Timestamp':'" + reader1["Timestamp"].ToString() +"'},");
                }
                sw.WriteLine("\t\t],\n\t'Run':\n\t\t[");
                reader1.Close();

                String query2 = "SELECT * FROM Run";
                SQLiteDataReader reader2 = con_qu.dataReader(query2);
                while (reader2.Read())
                {
                    string app= reader2["Application"].ToString().Replace("\\","...");
                    sw.WriteLine("\t\t{'Id':'" + reader2["Id"].ToString() + "','Application':'" + app + "','Timestamp':'" + reader2["Timestamp"].ToString() + "'},");
                }
                sw.WriteLine("\t\t]\n\t}\n}");
                reader2.Close();
               

            }
            sw.Close();

            StreamReader r = new StreamReader(path);


            string encripted = "";
            using (r)
            {
                string json = r.ReadToEnd();
                encripted= Encrypt(json);
                string decript = Decrypt(encripted);              
               
            }
            r.Close();
            StreamWriter rw = File.CreateText(path);

            using (rw)
            {
                rw.WriteLine(encripted);                
                MessageBox.Show("Successfully Exported", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            rw.Close();
               
        }

        public string slacerepace(string sl)
        {
            sl = sl.Replace("\\", "...");
            return sl;
        }

        public string tribledotrepace(string sl)
        {
            sl = sl.Replace("...", "\\");
            return sl;
        }

        public void import(string path)
        {
            
            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                try
                {
                    json = Decrypt(json);
                    json = json.Replace("'''", "'\''");
                    //MessageBox.Show(json);
                    JObject json1 = JObject.Parse(json);
                    toEmpty();

                    //try
                    //{
                    //    json = JToken.Parse(json).ToString();
                    //}
                    //catch (Exception e)
                    //{
                    //    MessageBox.Show("Wrong File Selection2", "Wrong!", MessageBoxButton.OK, MessageBoxImage.Error);
                    //    return;
                    //}


                    int i = json1["keyboard"]["Button"].Count();
                    int j = 0;
                    while (i > j)
                    {
                        string id = json1["keyboard"]["Button"][j]["Id"].ToString();
                        string name = tribledotrepace( json1["keyboard"]["Button"][j]["Name"].ToString()).Replace("___","''");
                       
                        string query = "INSERT INTO Button (Id,Name) VALUES('" + id + "','" + name + "') " ;

                        con_qu.executeQueries(query);


                        j++;
                    }
                    int k = json1["keyboard"]["MapString"].Count();
                    int l = 0;
                    while (k > l)
                    {
                        string id = json1["keyboard"]["MapString"][l]["Id"].ToString();
                        string button = tribledotrepace(json1["keyboard"]["MapString"][l]["Button"].ToString()).Replace("___", "''");
                        string map = tribledotrepace(json1["keyboard"]["MapString"][l]["MapString"].ToString());
                        string content = tribledotrepace (json1["keyboard"]["MapString"][l]["Content"].ToString());
                        string timestamp = json1["keyboard"]["MapString"][l]["Timestamp"].ToString();
                        string query1 = "INSERT INTO MapString (Id,Button,MapString,Content,Timestamp) VALUES('" + id + "','" + button + "','" + map + "','" + content + "','" + timestamp + "')";

                        con_qu.executeQueries(query1);


                        l++;
                    }

                    int m = json1["keyboard"]["Run"].Count();
                    int n = 0;
                    while (m > n)
                    {
                        string id = json1["keyboard"]["Run"][n]["Id"].ToString();
                        string app = json1["keyboard"]["Run"][n]["Application"].ToString().Replace("...", "\\");
                        string timestamp = json1["keyboard"]["Run"][n]["Timestamp"].ToString();
                        string query2 = "INSERT INTO Run (Id,Application,Timestamp) VALUES('" + id + "','" + app + "','" + timestamp + "')";

                        con_qu.executeQueries(query2);


                        n++;
                    }


                    MessageBox.Show("Succesfully Imported..","Success",MessageBoxButton.OK,MessageBoxImage.Information);
                    

                }
                catch (Exception e)
                {
                    MessageBox.Show("Wrong File Selection","Wrong!", MessageBoxButton.OK, MessageBoxImage.Error);
                }


            }
        }
        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "developed by Yahya & Suwarnakumar";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "developed by Yahya & Suwarnakumar";
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        public void toEmpty()
        {
            String querya = "DELETE FROM Mapstring";
            con_qu.executeQueries(querya);

            String query2 = "DELETE FROM Run";
            con_qu.executeQueries(query2);

            String query3 = "DELETE FROM Button";
            con_qu.executeQueries(query3);
          

        }


        public void showKeyContent(string btnName, string selectedkey,string windowname)
        {
			
			
			MainWindow m = new MainWindow("hai");
            ResetButtonWindow rb1 = new ResetButtonWindow();
           // btnName = replacesinglequote(btnName);
			var Id="";

			String qur = "SELECT * FROM Button WHERE Name='" +m.getKeyName(btnName, selectedkey) + "'";
            SQLiteDataReader dred = con_qu.dataReader(qur);
			
			while (dred.Read())
            {
                Id = dred["Id"].ToString();
				
			}
            dred.Close();
			if (Id == "")
			{
				MessageBox.Show("Field Was Null ", "", MessageBoxButton.OK, MessageBoxImage.Information);
			}
			else
			{
				string but1="";
				string content1="";
				string mapString1="";
				String qu1 = "SELECT * FROM Mapstring WHERE Id='" + Id + "'";
				SQLiteDataReader reader = con_qu.dataReader(qu1);
				
				while (reader.Read())
				{

					but1 = reader["Button"].ToString();
					content1 = reader["Content"].ToString();
					mapString1 = reader["MapString"].ToString();
					
				}
				reader.Close(); 



				string path1="";
				String qu2 = "SELECT * FROM Run WHERE Id='" + Id + "'";
				SQLiteDataReader reader1 = con_qu.dataReader(qu2);

				while (reader1.Read())
				{

					path1 = reader1["Application"].ToString();
				}
				reader1.Close();


				if (windowname == "window4")
				{
					
					ViewContent w4 = new ViewContent(but1, btnName, content1, mapString1, path1);
					w4.ShowDialog();
					w4.TopMost = true;
				}
				if (windowname == "reset")
				{
					rb1 = new ResetButtonWindow(but, btnName, content1, mapString1, path1, Id);
					rb1.ShowDialog();
					rb1.TopMost = true;
				}



			}
        }

        public void mappStringView()
        {
            

            String qu1 = "SELECT * FROM Mapstring";
            SQLiteDataReader reader = con_qu.dataReader(qu1);

            string[] btn = new string[1000];
            string[] mapString = new string[1000];
            string[] con = new string[1000];
            int i = 0;
           // string MessageBoxTitle = "Button";
            //string MessageBoxTitle1 = "mapsring";
            // string MessageBoxContent = mappstring;

            DataTable dt = new DataTable();

            while (reader.Read())
            {
                btn[i] = reader["Button"].ToString();
                mapString[i] = reader["MapString"].ToString();
                con[i] = reader["Content"].ToString();

                i++;
            }

            reader.Close();
            //MessageBox.Show(btn[1].ToString());
            ViewMappString v = new ViewMappString(btn, mapString, con);
            v.ShowDialog();
        }



        public void runView()
        {
            string[] id = new string[1000];
            string[] path = new string[1000];
            string[] button = new string[1000];

            String qu = "SELECT * FROM Run";
            SQLiteDataReader reader = con_qu.dataReader(qu);
            int i = 0;
            while (reader.Read()) {
                id[i] = reader["Id"].ToString();
                path[i] = reader["Application"].ToString();
                i++;
            }

            reader.Close();
            for (int j = 0; j < id.Length; j++)
            {
                if (id[j] != null)
                {
                    String qu1 = "SELECT * FROM Button WHERE id='" + id[j] + "'";
                    SQLiteDataReader reader1 = con_qu.dataReader(qu1);

                    while (reader1.Read())
                    {
                        button[j] = reader1["Name"].ToString();

                    }

                    reader1.Close();
                }
                else break;
            }

            ViewRun vr = new ViewRun(id, button, path);
            vr.ShowDialog();

        
        }



        public void keyboardRest(string btnName)
        {
            if (btnName != "")
            {
                

                if (MessageBox.Show("Are You Sure You Want Export?", "Export Current Data", MessageBoxButton.YesNo,MessageBoxImage.Question)==MessageBoxResult.Yes)
                {
                    Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                    dlg.FileName = "keyboard";
                    dlg.DefaultExt = ".kb";
                    dlg.Filter = "kb Files | *.kb";
                    Nullable<bool> result = dlg.ShowDialog();
                    if (result == true)
                    {

                        string filename = dlg.FileName;
                        export(filename);
                        //kbone.open(filename);
                    }
                    reset();
                }
                else
                {
                    reset();
                }

            }
            else
            {

                reset();

            }
        }

        public void cheackButtonName()
        {
            
                //here cheack the button is empty or not.

                String qu = "SELECT * FROM Button";
                SQLiteDataReader reader = con_qu.dataReader(qu);


                while (reader.Read())
                {
                    btnName = reader["Name"].ToString();

                }
                reader.Close();
                keyboardRest(btnName);
            


            //return btnName;


        }
        public string checkbutton()
        {
            String qu = "SELECT * FROM Button";
            SQLiteDataReader reader = con_qu.dataReader(qu);
            string b="";

            while (reader.Read())
            {
                b = reader["Name"].ToString();

            }
            reader.Close();
            return b;
        }

        public void reset()
        {
            String qu1 = "DELETE FROM Mapstring";
            con_qu.executeQueries(qu1);

            String qu2 = "DELETE FROM Run";
            con_qu.executeQueries(qu2);


            String qu3 = "DELETE FROM Button";
            con_qu.executeQueries(qu3);

            //File.WriteAllText(@"Test/Test.ahk", String.Empty);
            File.WriteAllText(@"Test/Test.ahk", String.Empty);
            //File.AppendAllText(@"Test/Test.ahk", "$A::" + Environment.NewLine);
            //File.AppendAllText(@"Test/Test.ahk", "\t send,a" + Environment.NewLine);
            File.AppendAllText(@"Test/Test.ahk", "Exit " + Environment.NewLine);

            //var a= System.Diagnostics.Process.Start(@"AutoHotkey.exe");
            foreach (var process in Process.GetProcessesByName("AutoHotkey"))
            {
                process.Kill();
            }
            //System.Diagnostics.kill(@"AutoHotkey.exe"();




        }


        public void addScript(string btn, string realName, string text, string content, string selectedkeyValue)
        {
            if (selectedkeyValue == "")
            {
                save(realName, content, text);

            }
            if (selectedkeyValue == "ctrl")
            {
                save("^" + realName, content, text);

            }
            if (selectedkeyValue == "AltCtrl")
            {

                save("!^" + realName, content, text);

            }
            if (selectedkeyValue == "ShiftCtrl")
            {
                save("+^" + realName, content, text);

            }
            if (selectedkeyValue == "Shift")
            {

                save("+" + realName, content, text);

            }
            
        }
    }
}

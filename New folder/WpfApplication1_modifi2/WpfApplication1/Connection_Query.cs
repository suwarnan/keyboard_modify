﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Data;
namespace WpfApplication1
{
    class Connection_Query
    {
        string conectionString = "Data Source=DB\\KeyMap.sqlite;Version=3;new=False;datetimeformat=CurrentCulture";
        SQLiteConnection con;
		SQLiteCommand cmd;

        public  Connection_Query()
        {
            con = new SQLiteConnection(conectionString);

            con.Open();
        }


        public void closeConnection()
        {
            con.Close();
        }

        public void executeQueries(string Query_)
        {
            //Insert , Update , Delete Queries (it is not return values)
            cmd = new SQLiteCommand(Query_ , con);
           cmd.ExecuteNonQuery(); 

        }

        public SQLiteDataReader dataReader(string Query_)
        {
            //Search Query(it is return value)
            cmd = new SQLiteCommand(Query_ , con);
            SQLiteDataReader dr = cmd.ExecuteReader(); 
            
            return dr; 
        }

        public object ShowDataInGridView(string Query_)
        {
            //show the data
            SQLiteDataAdapter dr = new SQLiteDataAdapter(Query_ , conectionString);
            DataSet ds = new DataSet();
            dr.Fill(ds);
            object dataum = ds.Tables[0];
            return dataum;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace WpfApplication1
{
    public partial class ViewMappString : Form
    {

        string[] btn;
        string[] mapString;
        string[] con ;
        public ViewMappString(string[] but, string[] mapstring, string[] con)
        {
            InitializeComponent();
            this.btn = but;
            this.con = con;
            this.mapString = mapstring;
            viewMapStringTable();
        }


        private void viewMapStringTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Button", typeof(string));
            dt.Columns.Add("MapString", typeof(string));
            dt.Columns.Add("Content", typeof(string));

            for (int i=0;i<btn.Length;i++) {

                if (btn[i] != null )
                {
                    dt.Rows.Add(new object[] { buttonMatch(btn[i]), mapString[i], con[i] });

                }
                else break;
            }
            dataGridView1.DataSource = dt;
            DataGridViewColumn column1 = dataGridView1.Columns[0];
            DataGridViewColumn column2 = dataGridView1.Columns[1];
            DataGridViewColumn column3 = dataGridView1.Columns[2];
            dataGridView1.Width = 800;
            column1.Width = 200;
            column2.Width = 300;
            column3.Width = 300;
        }


        private string buttonMatch(string a)
        {
           
            Regex re1 = new Regex(@"\!\^");
            Regex re2 = new Regex(@"\+\^");
            Regex re3 = new Regex(@"\^");
            Regex re4 = new Regex(@"\+");
            if (re1.IsMatch(a))
            {
                return "Alt+Ctrl+" + a.Remove(0, 2);
            }
            else if (re2.IsMatch(a))
            {
                return "Shift+Ctrl+" + a.Remove(0, 2);
            }
            else if (re3.IsMatch(a))
            {
                return "Ctrl+" + a.Remove(0, 1);
            }
            else if (re4.IsMatch(a))
            {
                return "Shift+"+a.Remove(0,1);
            }
            else {
                return a;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }

    }


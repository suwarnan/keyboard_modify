﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace WpfApplication1
{
    public partial class ViewRun : Form
    {
        string[] id;
        string[] name;
        string[] path;
        public ViewRun(string [] id, string[] name, string[] path)
        {
            InitializeComponent();
            this.id = id;
            this.name = name;
            this.path = path;
            ViewMapStringTable();
        }

        private void ViewMapStringTable()
        {
            DataTable dt = new DataTable();
            
            
            dt.Columns.Add("Button", typeof(string));
            dt.Columns.Add("Path", typeof(string));
           

            for (int i = 0; i < name.Length; i++)
            {
                if (name[i] != null)
                {
                    dt.Rows.Add(new object[] { ButtonMatch(name[i].ToString()), path[i] });
                }
                else break;     
            }
            dataGridView1.DataSource = dt;
            DataGridViewColumn column1 = dataGridView1.Columns[0];
            DataGridViewColumn column2 = dataGridView1.Columns[1];
            dataGridView1.Width = 800;
            column1.Width = 300;
            column2.Width = 500;
        }


        private string ButtonMatch(string a)
        {

            Regex re1 = new Regex(@"\!\^");
            Regex re2 = new Regex(@"\+\^");
            Regex re3 = new Regex(@"\^");
            Regex re4 = new Regex(@"\+");
            if (re1.IsMatch(a))
            {
                return "Alt+Ctrl+" + a.Remove(0, 2);
            }
            else if (re2.IsMatch(a))
            {
                return "Shift+Ctrl+" + a.Remove(0, 2);
            }
            else if (re3.IsMatch(a))
            {
                return "Ctrl+" + a.Remove(0, 1);
            }
            else if (re4.IsMatch(a))
            {
                return "Shift+" + a.Remove(0, 1);
            }
            else
            {
                return a;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}

    $(document).on("click", "#submit", function(evt)
    {
        /*global activate_page */
        var ses=localStorage.getItem("user_id_temp");
        if (ses===null){
            activate_page("#login");


        }
        else{
            activate_page("#navmenu");
            localStorage.getItem("user_id_temp");
            var cg=JSON.stringify(localStorage.getItem("fullname_temp"));
            profile_view();
        }
    });
 $(document).on("click", "#submit_log", function(evt)
    {
        $('#img').show(); 


        evt.preventDefault();
        var uname =  $('#username').val();
        localStorage.setItem('uname',uname);
        var password = $('#password').val();
        localStorage.setItem('password',password);
        $.ajax({
            url:webservice,
            type:"POST",
            dataType:"json",
            data:{type:"users",cmd:"login",email:uname,password:password},
            ContentType:"application/json",
            success: function(json){

                if (json.result==='success')
                {
                    $('#img').hide();

                    activate_page("#navmenu");
                    var Id=json.data.user_id;
                    localStorage.setItem("user_id_temp",Id);

                    profile_view();
                }
                else{
                    $('#img').hide();
                  
                    tempAlert("Username or password is incorrect",hide_durit);
                }

            },
            error: function(err){
                tempAlert("Connection Error",hide_durit);

            }
        });
        document.getElementById('username').value='';
        document.getElementById('password').value='';
    });
 function profile_view(){
        var $select = $('#showdata');
        var uname= localStorage.getItem("uname");
        var password= localStorage.getItem("password");
        $.ajax({
            url:webservice,
            type:"POST",
            dataType:"json",
            data:{type:"users",cmd:"login",email:uname,password:password},
            ContentType:"application/json",
            success: function(json){

                if (json.result==='success')
                    {$select.html('');
                $select.append('<div class="martop marbt"><div class="box nonebo"><div class="pic-style nonebor"><div class="also_timer ze"><img class="imglogo mar" src="'+json.data.photo+'"><p class="price planty">Business Name</p></div><img class="busspro" src='+json.data.photo+' style="background-size: 300px 300px;"></div></div><div class="busspros"><div class="leftdis">'+json.data.about+'</div><div class="aboutbuss"><div>'+json.data.street+'</div><div>'+json.data.city+'</div><div>'+json.data.phone+'</div><div>'+json.data.email+'</div><a href="http://'+json.data.website+'/"><div>'+json.data.website+'</div></a></div></div><div>');
            }
            else{
                $select.append("no data found");
            }
        },
        error: function(err){
            tempAlert("Connection Error",hide_durit);

        }
    });
    }
   $(document).on("click", "#img_1", function(evt)
    {
        /*global activate_page */

        activate_page("#profile");  
        viewpro(); 
    });
    function viewpro(){
        var $select = $('#profiles');
        $select.html('');

        var uname= localStorage.getItem("uname");
        var password= localStorage.getItem("password");
        $.ajax({
            url:webservice,
            type:"POST",
            dataType:"json",
            data:{type:"users",cmd:"login",email:uname,password:password},
            ContentType:"application/json",
            success: function(json){

                if (json.result==='success')
                {
                    $select.append('<div class="martop marbt"><div class="box nonebo"><div class="pic-style nonebor"><div class="also_timer ze"><img class="imglogo mar" src="'+json.data.photo+'"><p class="price planty">Business Name</p></div><img  id="output_profile" class="busspro" src='+json.data.photo+' style="background-size: 300px 300px;"><label class="fileUpload upimg"><input id="withphoto" type="file" class="upload" name="file" onchange="loadFileprofile(event)"><span>Upload picture</span></label></div></div><div class="busspros"><div class="leftdis"><textarea id="about" placeholder="About as" style="resize: none;" rows="4">'+json.data.about+'</textarea></div><div class="aboutbuss"><input type="text" value="'+json.data.street+'" id="street" placeholder="street"><input type="text" value="'+json.data.city+'" id="city" placeholder="city"><input type="text" value="'+json.data.phone+'" id="phone" placeholder="phone"><input type="text" value="'+json.data.email+'" id="emailid" placeholder="email"><a href="http://'+json.data.website+'/"><input type="text" value="'+json.data.website+'" id="website" placeholder="website"></a></div></div><div>');
                }
                else{
                    $select.append("no data found");
                }
            },
            error: function(err){
                tempAlert("Connection Error",hide_durit);

            }
        });
    }
   $(document).on("click", "#profile_btn", function(evt)
        {  evt.preventDefault();
            $('#img').show();
            var userid=localStorage.getItem("user_id_temp");
            var withphoto;

            var uid= parseInt(localStorage.getItem("user_id_temp"));

            var has_selected_file = $('input[type=file]').filter(function(){
                return $.trim(this.value) !== '';}).length  > 0 ;

            if (has_selected_file) {
                /* do something here */
                withphoto=1;
            }

            var about=$('#about').val();
            var street=$('#street').val();
            var city=$('#city').val();
            var phone=$('#phone').val();
            var emailid=$('#emailid').val();
            var website=$('#website').val();
            localStorage.setItem("uname",emailid);
            var formData = new FormData();
            formData.append('file', $('#withphoto')[0].files[0]);
            formData.append('type', "users");
            formData.append("cmd","user_update");
            formData.append('userid',userid);
            formData.append('about', about);
            formData.append('street', street);
            formData.append('city', city);
            formData.append('phone', phone);
            formData.append('emailid', emailid);
            formData.append('website', website);
            formData.append('withphoto', withphoto);

            $.ajax({
                url:webservice,
                type:"POST",
                dataType:"json",
                data:formData,
                ContentType:"application/json",
                processData: false,
                contentType: false,
                success: function(json){


                    if (json.result==='success')
                    {
                        $('#img').hide();
                         tempAlert("updated",hide_durit);
                        viewpro(); 


                    }
                    else{
                        $('#img').hide();
                  tempAlert("updated",hide_durit);
                        viewpro();
                    }


                },
                error: function(err){
                    tempAlert("Connection Error",hide_durit);
                }});


        });
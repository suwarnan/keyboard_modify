
      document.addEventListener("deviceready", init, false);
function init() {

    document.querySelector("#sendMessage").addEventListener("touchend", function() {
        console.log("click");
        var number = document.querySelector("#number").value;
        var message = document.querySelector("#message").value;
        console.log("going to send "+message+" to "+number);

        //simple validation for now
        if(number === '' || message === '') return;

        var msg = {
            phoneNumber:number,
            textMessage:message
        };

        sms.sendMessage(msg, function(message) {
            console.log("success: " + message);
            navigator.notification.alert(
                'Message to ' + number + ' has been sent.',
                null,
                'Message Sent',
                'Done'
            );

        }, function(error) {
            console.log("error: " + error.code + " " + error.message);
            navigator.notification.alert(
                'Sorry, message not sent: ' + error.message,
                null,
                'Error',
                'Done'
            );
        });

    }, false);

}/*
 * Please see the included README.md file for license terms and conditions.
 */


// This file is a suggested starting place for your code.
// It is completely optional and not required.
// Note the reference that includes it in the index.html file.


/*jslint browser:true, devel:true, white:true, vars:true */
/*global $:false, intel:false app:false, dev:false, cordova:false */



// This file contains your event handlers, the center of your application.
// NOTE: see app.initEvents() in init-app.js for event handler initialization code.

// function myEventHandler() {
//     "use strict" ;
// // ...event handler code here...
// }


// ...additional event handlers here...

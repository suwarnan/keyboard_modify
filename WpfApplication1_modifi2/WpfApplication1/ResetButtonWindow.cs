﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.SQLite;


namespace WpfApplication1
{
    public partial class ResetButtonWindow : Form
    {
        string but,con,mapstring,path,Id,button1;
        
        Connection_Query cq = new Connection_Query();
		keyboardOne kone = new keyboardOne();

		public ResetButtonWindow(string button,string button1, string content, string mapstring, string path,string Id)
        {
            InitializeComponent();
            this.but = button;
            this.con = content;
            this.mapstring = mapstring;
            this.path = path;
            this.Id = Id;

			this.button1 = button1;

           

            showContent();
        }

        public ResetButtonWindow()
        {
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string MessageBoxTitle = "View Path";
            string MessageBoxContent = path;

            if (path == "")
            {
                MessageBox.Show("Path Is Null Yet",MessageBoxTitle,MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(MessageBoxContent, MessageBoxTitle,MessageBoxButtons.OK,MessageBoxIcon.Information);

            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

		private void groupBox1_Enter(object sender, EventArgs e)
		{

		}

		private void button11_Click(object sender, EventArgs e)
        {
            
            this.Close();

        }

		private void button10_Click(object sender, EventArgs e)
		{
			if (checkBox1.Checked == false && checkBox2.Checked == false)
			{
				MessageBox.Show("Please Choose Checkbox", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);

			}
			else
			{
				System.Windows.Forms.DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Are You Sure You Want Remove Key ?", "Remove Current Key Status", System.Windows.Forms.MessageBoxButtons.YesNo,MessageBoxIcon.Information);
				if (dialogResult == System.Windows.Forms.DialogResult.Yes)
				{
					if (checkBox1.Checked == true && checkBox2.Checked == true)
					{
						String qu4 = "DELETE FROM Mapstring WHERE Id='" + Id + "'";
						cq.executeQueries(qu4);

						String qu5 = "DELETE FROM Run WHERE Id='" + Id + "'";
						cq.executeQueries(qu5);



						MessageBox.Show("delete All Data From Database", "", MessageBoxButtons.OK, MessageBoxIcon.Information);

						checkBox1.Checked = false;
						checkBox2.Checked = false;
					}
					else if (checkBox1.Checked == true)
					{

						if (Id != "")
						{
							String qu = "DELETE  FROM Mapstring WHERE Id='" + Id + "'";
							cq.executeQueries(qu);



							MessageBox.Show("Delete successfully", "", MessageBoxButtons.OK, MessageBoxIcon.Information);

							checkBox1.Checked = false;

						}

					}
					else if (checkBox2.Checked == true)
					{
						if (Id != "")
						{
							String qu6 = "DELETE  FROM Run WHERE Id='" + Id + "'";
							cq.executeQueries(qu6);

							MessageBox.Show("Delete successfully", "", MessageBoxButtons.OK, MessageBoxIcon.Information);

							checkBox2.Checked = false;


						}
					}
					String qu2 = "SELECT * FROM Mapstring WHERE Id='" + Id + "'";
					SQLiteDataReader reader = cq.dataReader(qu2);
					string Id1 = "";
					string Id2 = "";
					while (reader.Read())
					{
						Id1 = reader["Id"].ToString();

					}
					reader.Close();
					String qu1 = "SELECT * FROM Run WHERE Id='" + Id + "'";
					SQLiteDataReader reader1 = cq.dataReader(qu1);

					while (reader1.Read())
					{
						Id2 = reader1["Id"].ToString();
					}
					reader1.Close();
					if (Id1 == "" && Id2 == "")
					{
						String qu3 = "DELETE FROM Button WHERE Id='" + Id + "'";
						cq.executeQueries(qu3);
					}

				}
				System.Windows.Forms.Application.Restart();


			}
		}
       

        private void button4_Click(object sender, EventArgs e)
        {
            string MessageBoxTitle = "Mapping Sting";
            string MessageBoxContent = mapstring;
            // MessageBox.Show(MessageBoxContent, MessageBoxTitle, MessageBoxButtons.YesNo);
            if (mapstring == "")
            {
                MessageBox.Show("Mapstring Is Null Yet",MessageBoxTitle,MessageBoxButtons.OK,MessageBoxIcon.Information);

            }
            else
            {
                MessageBox.Show(MessageBoxContent, MessageBoxTitle,MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
           

        }

        private void showContent()
        {
			button1 = kone.changeToSingelQuotation(button1);

			if (con != "")
			{
				label2.Text = button1;
				label3.Text = con;
				label1.Text = button1;
				label4.Text = button1;
			}
			else
			{
				label2.Text = button1;
				label3.Text = button1;
				label1.Text = button1;
				label4.Text = button1;
			}
		}


    }
}
